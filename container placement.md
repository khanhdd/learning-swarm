# Container Placement 
## Node labels + Service constraints 
* Hard requirement. Only schedules task if Swarm matches constraint
* Add labels to nodes first, based on location, hardware, or purpose
* Then use constraints when creating services

## Service Modes: Replicated (--replicas) or Global (one per node)
* Global is good for proxy/monitoring/logging/security tools
* Only set at service create time

## Placement Preferences
* Soft requirement. For now only used to spread across availability zones

## Node Availability
* Three options: active, pause, drain

## Resource Requirements
* --reserve-cpu --reserve-memory for min, --limit-cpu --limit-memory for max

# Usage 
```bash
#1. Node labels + service constraints
# place only on a manager 
docker service create --constraint=node.role==manager nginx
docker service create --constraint=node.role!=worker nginx
# add label to node2 for dmz=true and constraint to it
docker node update --label-add=dmz=true node2
docker service create --constraint=node.labels.dmz==true nginx

#2. Service modes
# place one task on each node in Swarm
docker service create --mode=global nginx
# place one task on each Worker in Swarm
docker service create --mode=global --constraint=node.role ==worker nginx

#3. Placement preferences
# labels all your AWS nodes with AZs
docker node update --label-add=azone=a nodeX
docker node update --label-add=azone=b nodeY
docker node update --label-add=azone=c nodeZ
# make sure your service is deployed to all AZs
docker service create --placement-pref spread=node.labels.azone --replicas 3 nginx
# user service update to add/remove preferences
--placement-pref-add spread=node.labels.subnet
--placement-pref-rm spread=node.labels.subnet
# multi-layer preferences
https://docs.docker.com/engine/swarm/services/

#4. Node availability
# prevent node2 from starting new containers
docker node update --availability pause node2
# stop containers on node3 and assign their tasks to other nodes
docker node update --availability drain node3

#5. Resource requirements
# reserve cou and memory
docker service create --reserve-memory 800M --reserve-cpu 1 mysql
# limit cpu and memory
docker service create --limit-memory 150M --limit-cpu .25 nginx
# service update is the same
```