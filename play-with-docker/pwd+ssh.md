# Play with docker

* Generate ssh keygen in bash:
```bash
    ssh-keygen -t rsa -P "" -f ~/.ssh/id_rsa
```

* Copy public key to pwd docker-machine:
```bash
scp id_rsa.pub <instance_ip_with_dashes>-<short_session_id>@direct.labs.play-with-docker.com:.ssh/authorized_keys
```

*  SSH to pwd:
```bash
ssh <instance_ip_with_dashes>-<short_session_id>@direct.labs.play-with-docker.com
```