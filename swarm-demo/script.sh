#!/bin/bash
# sleep until instance is ready
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
  done
  #container up
  cd /tmp/ \
  && sudo apt-get update \
  && curl -fsSL https://get.docker.com -o get-docker.sh \
  && sh get-docker.sh
  && sudo usermod -aG docker ubuntu